# 1 Introduction  

Recursive Ray Tracing Animation for a Set of Spheres in Real-Time

Ray Tracing Animation is an animation rendering technique based on ray tracing technology. Compared to traditional rendering methods like rasterization, ray tracing enables the creation of more realistic and lifelike visual effects. Ray Tracing Animation applies this highly realistic rendering technique to the production of animated scenes, offering realistic lighting, shadows, reflections, and refractions.

Here are some key features and advantages of Ray Tracing Animation:

Realistic lighting simulation: Ray tracing accurately simulates the propagation and interaction of light rays, resulting in realistic lighting effects. Light sources, materials, and ambient lighting in the animation can be accurately simulated, creating lifelike lighting effects in the scene.

High-quality shadow effects: Through ray tracing, precise calculations of shadow generation and projection can be performed. This enhances the accuracy and naturalness of shadow effects in the animation, providing better spatial perception and object relationships.

Accurate reflections and refractions: Ray tracing technology calculates the reflection and refraction processes of light rays on object surfaces with high precision. This enables realistic mirror reflections, refractions, and transparency effects in the animation, adding interactions and details between objects.

Applicable to various scenes: Ray tracing is suitable for various types of scenes, including indoor, outdoor, natural landscapes, virtual reality, and more. It can render complex lighting effects such as global illumination, indirect lighting, and ambient occlusion, making the animated scenes more realistic and visually captivating.

Customizability and scalability: Ray Tracing Animation offers flexible parameter adjustments and scene settings, allowing customization and expansion based on specific requirements. Users can adjust parameters such as light sources, materials, camera settings, etc., to meet different creative and visual effect demands.

Ray Tracing Animation is widely used in fields such as film, gaming, advertising, and animation production. It provides highly realistic and visually stunning effects, making animated scenes more immersive and attention-grabbing. With ongoing advancements in computer hardware and ray tracing algorithms, Ray Tracing Animation will continue to be an important technology and creative tool in the field of animation production.  

## Screenshots
![alt tag](https://raw.githubusercontent.com/dybiszb/GPURayTracer/master/img/scr1.png)


## Compiling<a name="compile"></a>
Note: Only UNIX compilation is allowed.

In repository main folder call:

```
make all
```

One executable called 'main' should be created.

# 2 Command   

## Run<a name="run"></a>
 
To start with the application can work in two different modes:
- CPU - runs calculations on a 'classic' processor.
- GPU - runs calculations on a 'graphics' processor.

In order to run the former call:
```
$ ./main cpu [number of spheres]  

```

The latter one can be invoked via:
```
$ ./main gpu [number of spheres]  
e.g. 
$ ./main gpu 100
```

