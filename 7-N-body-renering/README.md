# 1 Introduction
The code implements a simulation of an N-body system and renders the particles on a screen. Here is a high-level description of what the code does:

The code defines a class called NBodyRenderer that is responsible for rendering the particles of an N-body system. It supports two modes of operation: software rendering and CUDA-accelerated rendering.

In the constructor of the NBodyRenderer class, a set of particles is generated using a UniformRandomParticleGenerator. The particle positions, velocities, and other attributes are stored in arrays. The constructor also allocates memory for the frame buffer that will store the rendered image.

The class provides two update functions: update_software and update_cuda. The update_software function iterates over each pixel in the frame buffer and calculates the brightness based on the distance between the pixel and each particle. The resulting brightness values are used to set the color of the pixel in the frame buffer.

The update_cuda function performs the same calculations as update_software but utilizes CUDA for parallel processing. CUDA kernels (cuda_accelerate, cuda_move, and cuda_render) are launched to accelerate the particle movement, update their positions, and render them on the frame buffer. CUDA-specific constructs such as thread blocks and synchronization are used.

The NBodyRenderer class also provides getter functions for the width and height of the rendered image, the size of the frame buffer, and the buffer itself.

In summary, this code implements an N-body simulation and provides rendering capabilities using either software or CUDA acceleration. It calculates the positions and velocities of particles, updates them over time, and renders the resulting image.

# 2 Command 
./nbody 

