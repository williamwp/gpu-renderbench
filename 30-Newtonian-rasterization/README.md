# 1 Introduction
You need the SDL1.2, freeglut and CUDA libraries to compile. To install the first two on ubuntu, run the following:

apt-get install libsdl1.2-dev libsdl-image1.2-dev freeglut3-dev

Installing CUDA is a pain in the neck, unfortunately. You need to install both the Nvidia driver and the CUDA libraries. Instructions to do this are provided here : http://docs.nvidia.com/cuda/cuda-installation-guide-linux/#axzz4g7polGbZ

Once these packages are installed, run make to compile and ./bhfs to run the program

Once in the program, use 'm' to switch between Newtonian and Relativistic modes. Newtonian rendering just uses raster operations and is thus super fast.
Note : I think I goofed on the sign of phi, so left arrow will take you right and vice versa in the GR mode.

Arrow keys change theta and phi (distance to the Black Hole is fixed in GR mode)

'p' prints out current theta and phi for the camera

Spacebar prints the frame rate

# 2 Command 
./bhfs
